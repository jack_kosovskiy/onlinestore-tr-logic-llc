﻿
<table border="0" 
style="
background: #DCFFD6;
color: #000;
border: solid 1px #989898;
border-radius: 20px;
overflow: hidden;
padding: 30px 30px 40px 30px;
display: inline-block;
box-shadow: 4px 4px 3px rgba(0,0,0,0.2);
">


  <tr>
    <td colspan="2"><strong>Письмо с моего сайта:</strong></td>
  </tr>
  <tr>
    <td>%%name.title%%</td>
    <td>%%name.value%%</td>
  </tr>
  <tr>
    <td>%%e-mail.title%%</td>
    <td>%%e-mail.value%%</td>
  </tr>
  <tr>
    <td>%%text.title%%</td>
    <td>%%text.value%%</td>
  </tr>
</table>
